package com.internshipproject.controller;

import com.internshipproject.model.Filtration;
import com.internshipproject.service.FiltrationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/filtrations")
@CrossOrigin(origins = "*")
public class FiltrationController {

    @Autowired
    private FiltrationService filtrationService;

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveFiltration(@RequestBody Filtration filtration) {
        filtrationService.saveFiltration(filtration);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Filtration> update(@PathVariable(value = "id") Integer id, @RequestBody Filtration filtration) {
        return new ResponseEntity<>(filtrationService.update(id, filtration), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable(value = "id") Integer id) {
        filtrationService.delete(id);
    }


    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<List<Filtration>> findAll() {
        return new ResponseEntity<>(filtrationService.getAll(), HttpStatus.OK);
    }


}