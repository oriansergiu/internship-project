package com.internshipproject.exception;

import org.springframework.http.HttpStatus;

public class InvalidEmailAddress extends InternshipException {

    public InvalidEmailAddress(String messageKey, HttpStatus status, String... arguments) {
        super(messageKey, status, arguments);
    }
}