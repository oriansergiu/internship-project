package com.internshipproject.repository;

import com.internshipproject.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

    @Query("SELECT us from User us where us.username = :username")
    User findByUsername(@Param("username") String username);
}
