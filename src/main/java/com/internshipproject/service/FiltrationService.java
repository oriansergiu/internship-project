package com.internshipproject.service;

import com.internshipproject.model.Filtration;
import com.internshipproject.repository.FilterRepository;
import com.internshipproject.repository.FiltrationRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FiltrationService {

    @Autowired
    private FiltrationRepository filtrationRepository;

    public void saveFiltration(Filtration filtration) {

        Filtration oldFiltration = filtrationRepository.findByUsername(filtration.getUsername());

        if (oldFiltration != null) {
            filtration.setId(oldFiltration.getId());
            filtrationRepository.save(filtration);
        } else {
            filtrationRepository.save(filtration);
        }
    }

    public Filtration update(Integer id, Filtration filtration) {
        filtration.setId(id);
        filtrationRepository.save(filtration);
        return filtration;
    }

    public void delete(Integer id) {
        filtrationRepository.delete(id);
    }

    public List<Filtration> getAll() {
        return (List<Filtration>) filtrationRepository.findAll();
    }

}