package com.internshipproject.repository;

import com.internshipproject.model.Filtration;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FiltrationRepository extends CrudRepository<Filtration, Integer> {

    @Query("SELECT filt from Filtration filt where filt.username = :user")
    Filtration findByUsername(@Param("user") String user);
}